package com.latihan.demosecurityjwt;


import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class DemoSecurityJwtApplicationTests {

	@Test
	void contextLoads() {
	}

}
