package com.latihan.demosecurityjwt.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.latihan.demosecurityjwt.domain.Role;

public interface RoleRepo extends JpaRepository<Role,Long> {
    Role findByName (String name);
}
