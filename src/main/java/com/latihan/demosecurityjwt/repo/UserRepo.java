package com.latihan.demosecurityjwt.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.latihan.demosecurityjwt.domain.User;

public interface UserRepo extends JpaRepository<User, Long>{
    User findByUsername(String username);    
}
